$('#slider__wrapper').coinslider({width: 1000, height: 420});

//mobile slider
$('.mobile__slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    arrows: false,
    // prevArrow: $('.mobile__slider__prev'),
    // nextArrow: $('.mobile__slider__next')

});

//slider porfolio
$('.portfolio-slider__wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    lazyLoad: 'ondemand',
    prevArrow: $('.portfolio-slider__prev'),
    nextArrow: $('.portfolio-slider__next'),
    responsive: [
        {
            breakpoint: 760,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 540,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});



//popup
$("#phone_1").mask("+7 (999) 999-99-99");




//mobile menu
var btnMenu = $("#menu__toggle");


btnMenu.on('click', ()=>{
    $(".main-content").addClass('header-active');
    $(".mm").css('z-index', 10000);
    $(".mm__close").addClass('overlay');
    $(".mm__menu").addClass('visibility');
    $(".main-wrapper").addClass('scroll');

})

    $(".mm__close").on('click', () => {

        // $(".mm").removeClass('display');
        $(".main-content").removeClass('header-active');
        $(".mm").css('z-index', 100);
        $(".mm__close").removeClass('overlay');
        $(".main-wrapper").removeClass('scroll');
        $(".mm__menu").removeClass('visibility');
    });

lightbox.option({
    'resizeDuration': 200,
    'wrapAround': true
})


/* myLib start */
;(function() {
    window.myLib = {};
    window.myLib.body = document.querySelector('body');
    window.myLib.closestAttr = function(item, attr) {
        var node = item;
        while(node) {
            var attrValue = node.getAttribute(attr);
            if (attrValue) {
                return attrValue;
            }
            node = node.parentElement;
        }
        return null;
    };
    window.myLib.closestItemByClass = function(item, className) {
        var node = item;
        while(node) {
            if (node.classList.contains(className)) {
                return node;
            }
            node = node.parentElement;
        }
        return null;
    };
    window.myLib.toggleScroll = function() {
        myLib.body.classList.toggle('no-scroll');
    };
})();
/* myLib end */

/* popup start */
;(function() {
    var showPopup = function(target) {
        target.classList.add('is-active');
    };
    var closePopup = function(target) {
        target.classList.remove('is-active');
    };

    myLib.body.addEventListener('click', function(e) {
        var target = e.target;
        var popupClass = myLib.closestAttr(target, 'data-popup');

        if (popupClass === null) {
            return;
        }
        e.preventDefault();
        var popup = document.querySelector('.' + popupClass);

        if (popup) {
            showPopup(popup);
            myLib.toggleScroll();
        }
    });
    myLib.body.addEventListener('click', function(e) {
        var target = e.target;
        if (target.classList.contains('popup-close') ||
            target.classList.contains('popup__inner')) {
            var popup = myLib.closestItemByClass(target, 'popup');
            closePopup(popup);
            myLib.toggleScroll();
        }
    });

    myLib.body.addEventListener('keydown', function(e) {
        if (e.keyCode !== 27) {
            return;
        }
        var popup = document.querySelector('.popup.is-active');
        if (popup) {
            closePopup(popup);
            myLib.toggleScroll();
        }
    });
})();
